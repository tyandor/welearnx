(function ($, root, undefined) {

$(function () {

    'use strict';

    console.log("Admit it.");

    var controller = new ScrollMagic.Controller();

    // animate the element arrows
    var elementsIn = new TimelineMax();

    elementsIn.insertMultiple([
        new TweenMax.from($('.element-one'), 1, {x:2500, scale:2, ease:Back.easeIn.config(1.7)}),
        new TweenMax.from($('.element-two'), 1.5, {x:2500, scale:2, ease:Back.easeIn.config(1.7)}),
        new TweenMax.from($('.element-three'), 2, {x:2500, scale:2, ease:Back.easeIn.config(1.7)})
    ]);

    var triggerElementsIn = new ScrollMagic.Scene({
        triggerElement: '.lesson-list',
        offset: -350,
        duration: "75%"
    })
    .setTween(elementsIn)
    .addTo(controller);

    // animate the brain
    function testCallback() {
        console.log("Called back."); 
    }

    new ScrollMagic.Scene({
        triggerElement: ".spaced-learning",
        triggerHook: 0,
        duration: "200%",
        offset: 500
    })
    .setPin(".brain")
    .setClassToggle(".brain", "pinned")
    .on("enter leave", testCallback)
    .addIndicators()
    .addTo(controller);


});

})(jQuery, this);

