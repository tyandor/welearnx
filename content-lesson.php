<?php
/**
 * @version 6.4.3
 */
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?> <?php wpforge_article_schema( 'CreativeWork' ); ?>>

    <header class="lesson-header">
        <img src="<?php bloginfo('template_url'); ?>/images/img_header_hero.svg">
        <?php the_title( '<h1 class="entry-title-post" itemprop="headline">', '</h1>' ); ?>
        <div class="done-what">
            Done what?
            <div class="arrow-down"></div>
        </div>
    </header>

    <?php

    // check if the flexible content field has rows of data
    if( have_rows('content_blocks') ):

        // loop through the rows of data
        while ( have_rows('content_blocks') ) : the_row();

        if( get_row_layout() == 'text_block' ): ?>

            <section class="lesson-intro">
                <div class="grid-x">
                    <div class="small-12 medium-6 medium-offset-3 cell">
                        <?php the_sub_field('text_block_content'); ?>
                    </div>
                </div>
            </section>

        <?php endif;

        if( get_row_layout() == 'video' ): 

            the_sub_field('embed_code');

        endif;

        if( get_row_layout() == 'image' ): ?>

            <section class="large-image">
                <?php $image = get_sub_field('large_image'); ?>
                <img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt'];
        ?>" />
            </section>

        <?php endif;

        endwhile;

    else :

        // no layouts found

    endif;

    ?>


    <div class="grid-x">
        <div class="small-12 text-center cell">
            <p class="lead">When you’re creating your corporate learning story, follow these key plot elements:</p>
        </div>
    </div>

    <section class="lesson-list">
        <ul>
            <li class="element-one">
                <div class="grid-x">
                    <div class="small-10 medium-9 cell">
                        <h3>Attention is critical</h3>
                        <p>We hear it all the time – our attention spans are getting shorter. On average, attention spans are about 5 seconds. While we obviously can’t keep any sort of learning that short, keep in mind that holding the learner’s attention with a binge worthy content.</p>
                    </div>
                </div>
            </li>
            <li class="element-two">
                <div class="grid-x">
                    <div class="small-10 medium-9 cell">
                        <h3>Generating insights takes time</h3>
                        <p>Learning is a journey, just like getting to know your favorite TV characters. In order for learns to value the content and apply it to their own situations, they need time to digest it. It doesn’t happen overnight.</p>
                    </div>
                </div>
            </li>
            <li class="element-three">
                <div class="grid-x">
                    <div class="small-10 medium-9 cell">
                        <h3>Emotions govern</h3>
                        <p>We know it’s true – who doesn’t love a good Cinderella story? The emotional aspects of our favorite stories are instrumental keeping us engaged. The more we are able to connect emotions back to what we are learning, the more it will stick.</p>
                    </div>
                </div>
            </li>
        </ul>
    </section>

    <section class="spaced-learning">
        <div id="brain-trigger"></div>
        <div class="grid-x">
            <div class="small-7 small-offset-1 cell">
                <h2>Spaced learning sticks</h2>
                <p>Psychologists have been saying it for years – people learn best when learning is spaced out over several different sessions. Providing learnings in episodes will not only make it easier for learners to digest, but will hopefully make them eager to come back for more!</p>
                <p>When you put it like that, designing a binge worthy learning campaign sounds manageable – and it is-but I would be misleading you if I didn’t admit there are times when even this method can fail. By borrowing a few best practices from our friends in the marketing world, we can assure they keep coming back for more.</p>
                <ul>
                    <li><strong>Put Courses Where People are Looking</strong> &nbsp; Make sure courses are accessible to learners – meet them where THEY are, don’t make them come to you. (think streaming, cloud based,) or (think prime time slots, great lead ins, and not buried down the channel lineup).</li>
                    <li><strong>Design Courses Around Learners</strong> &nbsp; Some marketers will tell you that content is king, but not if the learner can’t relate. Courses should be designed around the learners! (think about how Netflix, amazon, and other streaming services now wrap around you and your viewing tastes).</li>
                    <li><strong>Be Creative When it Comes to Constraints</strong> &nbsp; Things like time and budget constraints often get in the way of the best learning designs. Be creative – learning doesn’t have a start and stop time, it should be continuous. (think how channels now make multiple seasons available).</li>
                    <li>
                        <strong>Courses Experience Fades</strong> &nbsp; Guides end up on shelves and lost in LMS, online learning can be a journey that lives on like marketing materials do. (think about the growing number of fan talk shows and web sites that keep the conversation going well after the show aired).
                    </li>
                </ul>
            </div>
            <div class="small-4 cell">
                <img class="brain" src="<?php bloginfo('template_url'); ?>/images/big-brain.svg" alt="big brain">
            </div>
        </div>
    </section>

    <section class="lesson-conclusion">
        <div class="grid-x">
            <div class="small-3 cell">
                <div class="red-pointer"></div>
            </div>
            <div class="small-7 cell">
                <p class="lead">I know what you’re thinking – even if I have this star-studded course line up, how do I get my zombie-filled conference room to focus on the lesson at hand? In my next post, I’ll share more about getting (and keeping) learners’ full attention.</p>
                <p class="lead">With these things in mind, designing a successful learning campaign shouldn’t feel like rocket-science, it should be enjoyable, intuitive and ultimately inspiring to your learners.</p>
            </div>
        </div>
    </section>


    <!-- <header class="entry-header">
        <?php  if( get_theme_mod( 'wpforge_cat_display','yes' ) == 'yes') { ?>
        <?php  if( get_theme_mod( 'wpforge_cat_position','top' ) == 'top') { ?>
        <?php wpforge_entry_meta_categories(); ?>
        <?php } // end if ?>
        <?php } // end if ?>
        <?php if ( is_single() ) : ?>
        <?php the_title( '<h1 class="entry-title-post" itemprop="headline">', '</h1>' ); ?>
        <?php else : ?>
        <?php the_title( sprintf( '<h2 class="entry-title-post" itemprop="headline"><a href="%s" rel="bookmark">', esc_url( get_permalink() ) ), '</a></h2>' ); ?>
        <?php endif; // is_single() ?>
        <div class="entry-meta-header">
            <?php  if( get_theme_mod( 'wpforge_meta_display','yes' ) == 'yes') { ?>
            <?php wpforge_entry_meta_header(); ?>
            <?php } // end if ?>
            <?php if ( comments_open() ) : ?>
            <span class="genericon genericon-comment"></span> <?php comments_popup_link( '<span class="leave-reply">' . __( 'Comment', 'wp-forge' ) . '</span>', __( '1 Comment', 'wp-forge' ), __( '% Comments', 'wp-forge' ) ); ?>
            <?php edit_post_link( __( 'Edit', 'wp-forge' ), '<span class="edit-link"><span class="genericon genericon-edit"></span>','</span>' ); ?>
            <?php else : ?>
            <?php edit_post_link( __( 'Edit', 'wp-forge' ), '<span class="edit-link-none"><span class="genericon genericon-edit"></span>','</span>' ); ?>
            <?php endif; // comments_open() ?>
        </div>
        <?php if ( get_theme_mod('wpforge_thumb_display','yes') == 'yes') : ?>
        <?php if ( is_front_page() || is_home() || is_archive() || is_search() || is_tag() ) : ?>
        <?php the_post_thumbnail(); ?>
        <?php endif; // end if ?>
        <?php endif; // end if ?>
        <?php if ( get_theme_mod('wpforge_single_thumb_display','yes') == 'yes') : ?>
        <?php if ( is_single() ) : ?>
        <?php the_post_thumbnail(); ?>
        <?php endif; // end if ?>
        <?php endif; // end if ?>
    </header> .entry-header -->
    <?php if ( is_single() ) : ?>
    <div class="entry-content-post" itemprop="text">
        <?php the_content( __( 'Continue reading <span class="meta-nav">&raquo;</span>', 'wp-forge')); ?>
        <?php wp_link_pages( array( 'before' => '<div class="page-links">' . __('Pages:','wp-forge'), 'after' => '</div>' ) ); ?>
    </div><!-- .entry-content -->
    <?php endif; ?>

    <footer class="entry-meta">
        <div class="entry-meta-footer">
            <?php  if( get_theme_mod( 'wpforge_cat_display' ) == 'yes') { ?>
            <?php  if( get_theme_mod( 'wpforge_cat_position' ) == 'bottom') { ?>
            <?php wpforge_bottom_meta_categories(); ?>
            <?php } // end if ?>
            <?php } // end if ?>
            <?php  if( get_theme_mod( 'wpforge_tag_display','yes' ) == 'yes') { ?>
            <?php wpforge_entry_meta_footer(); ?>
            <?php } // end if ?>
        </div><!-- end .entry-meta-footer -->
        <?php get_template_part( 'content', 'author' ); ?>
    </footer><!-- .entry-meta -->

</article>
